(function () {
  let footerColorContrast = "dark"; // default value

  // query footers recursively
  function getFooterDOM() {
    const footersWithClass = Array.from(document.querySelectorAll(".footer"));
    const footersWithTag = Array.from(document.querySelectorAll("footer"));
    return footersWithClass.length
      ? footersWithClass
      : footersWithTag.length
      ? footersWithTag
      : getFooterDOM();
  }

  // create link
  function genreateLinkDOM() {
    const referrer = window.location.origin;
    const referrer_link = "https://lo.agency" + "?dr=" + referrer;
    const link = document.createElement("a");

    // set links attributes
    link.setAttribute("href", referrer_link);
    link.setAttribute("target", "_blank");
    link.setAttribute("class", "lo-signature");
    link.setAttribute("rel", "noreferrer");

    // create copyright
    const copyRightText = "Developed By Lo Agency";
    const copyright = document.createElement("span");

    // set copyright attributes
    copyright.textContent = copyRightText;
    copyright.setAttribute("class", "lo-copyright");

    // create lo circles
    const circles = document.createElement("div");
    const bigCircle = document.createElement("span");
    const smallCircle = document.createElement("span");

    // set circles attributes
    circles.setAttribute("class", "lo-circles");
    bigCircle.setAttribute("class", "lo-c1");
    smallCircle.setAttribute("class", "lo-c2");

    // appending circles
    circles.appendChild(bigCircle);
    circles.appendChild(smallCircle);

    // appending all into one
    link.appendChild(copyright);
    link.appendChild(circles);

    return link;
  }

  // if pushState fired
  var pushState = history.pushState;
  history.pushState = function () {
    pushState.apply(history, arguments);

    // append the DOM again
    let timer = setTimeout(() => {
      appendSignatureDOM();
      clearTimeout(timer);
    }, 500);
  };

  // get backgroundColor of footer
  function getBackgroundColor(computedStyles) {
    if (!computedStyles.length) return "";

    const colors = computedStyles.map((item) => {
      return (
        item.getPropertyValue("background-color") ||
        item.getPropertyValue("background")
      );
    });

    return [...new Set(colors)]; // remmove duplicate items
  }

  // append DOM
  function appendSignatureDOM() {
    // query footer and its ComputedStyle
    let footers = getFooterDOM();
    const computedStyles = Array.from(footers).map((footer) =>
      window.getComputedStyle(footer)
    );

    const color = getBackgroundColor(computedStyles)[0];
    footerColorContrast = lightOrDark(color);

    if (footers) {
      footers.forEach((footer, index) => {
        const currentPosition = computedStyles[index].getPropertyValue(
          "position"
        );
        if (!currentPosition || currentPosition === "static") {
          footer.style.position = "relative";
        }

        const linkToAppend = genreateLinkDOM();
        footer.appendChild(linkToAppend);
      });
    }
  }

  // detect the color contrast of footer
  function lightOrDark(color) {
    // Check the format of the color, HEX or RGB?
    if (color.match(/^rgb/)) {
      // If HEX --> store the red, green, blue values in separate variables
      color = color.match(
        /^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/
      );

      r = color[1];
      g = color[2];
      b = color[3];
    } else {
      // If RGB --> Convert it to HEX: http://gist.github.com/983661
      color = +(
        "0x" + color.slice(1).replace(color.length < 5 && /./g, "$&$&")
      );

      r = color >> 16;
      g = (color >> 8) & 255;
      b = color & 255;
    }

    // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
    hsp = Math.sqrt(0.299 * (r * r) + 0.587 * (g * g) + 0.114 * (b * b));

    // Using the HSP value, determine whether the color is light or dark
    if (hsp > 127.5) {
      return "light";
    } else {
      return "dark";
    }
  }

  window.addEventListener("DOMContentLoaded", () =>
    setTimeout(() => {
      // append the DOM
      appendSignatureDOM();

      // append styles
      const style = document.createElement("style");
      style.setAttribute("id", "lo_signature_styles");

      style.innerHTML = `
  .lo-signature {
  position: absolute;
  bottom: 1.2rem;
  right: 1.2rem;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 12px;
  text-decoration: none !important;
  opacity: 0.3;
  }
  .lo-signature .lo-copyright {
  display: block;
  color: ${footerColorContrast === "dark" ? "#fff" : "#000"};
  opacity: 1;
  transform: scaleX(1);
  animation-name: copyright-out;
  animation-timing-function: cubic-bezier(0.25, 1, 0.5, 1);
  animation-duration: 500ms;
  animation-delay: 700ms;
  animation-fill-mode: backwards;
  }
  .lo-signature .lo-circles {
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  transform: scale(0);
  opacity: 0;
  animation-name: circles-out;
  animation-timing-function: cubic-bezier(0.64, 0.57, 0.67, 1.53);
  animation-duration: 400ms;
  animation-delay: 300ms;
  animation-fill-mode: backwards;
  }
  .lo-signature .lo-circles span {
  position: absolute;
  background-color: ${footerColorContrast === "dark" ? "#fff" : "#000"};
  border-radius: 999px;
  }
  .lo-signature .lo-circles .lo-c1 {
  width: 32px;
  height: 32px;
  }
  .lo-signature .lo-circles .lo-c2 {
  width: 10.66px;
  height: 10.66px;
  transform: translate(0, 0);
  animation-name: smallcircle-out;
  animation-timing-function: cubic-bezier(0.64, 0.57, 0.67, 1.5);
  animation-duration: 300ms;
  animation-delay: 0s;
  animation-fill-mode: forwards;
  }
  /* hover */
  .lo-signature:hover .lo-copyright {
  animation-name: copyright-in;
  animation-timing-function: cubic-bezier(0.25, 1, 0.5, 1);
  animation-duration: 500ms;
  animation-delay: 0s;
  animation-fill-mode: forwards;
  }
  .lo-signature:hover .lo-circles {
  animation-name: circles-in;
  animation-timing-function: cubic-bezier(0.64, 0.57, 0.67, 1.5);
  animation-duration: 400ms;
  animation-delay: 500ms;
  animation-fill-mode: forwards;
  }
  .lo-signature:hover .lo-circles .lo-c2 {
  animation-name: smallcircle-in;
  animation-timing-function: cubic-bezier(0.64, 0.57, 0.67, 1.5);
  animation-duration: 300ms;
  animation-delay: 900ms;
  animation-fill-mode: forwards;
  }
  @keyframes copyright-in {
  from {
      transform: scaleX(1);
      opacity: 1;
  }
  to {
      transform: scaleX(0);
      opacity: 0.5;
  }
  }
  @keyframes copyright-out {
  from {
      transform: scaleX(0);
      opacity: 0.5;
  }
  to {
      transform: scaleX(1);
      opacity: 1;
  }
  }
  @keyframes circles-in {
  from {
      opacity: 0;
      transform: scale(0);
  }
  to {
      opacity: 1;
      transform: scale(1);
  }
  }
  @keyframes circles-out {
  from {
      opacity: 1;
      transform: scale(1);
  }
  to {
      opacity: 0;
      transform: scale(0);
  }
  }
  @keyframes smallcircle-in {
  form {
      transform: translate(0, 0);
  }
  to {
      transform: translate(29px, 0);
  }
  }
  @keyframes smallcircle-out {
  from {
      transform: translate(29px, 0);
  }
  to {
      transform: translate(0, 0);
  }
  }`;
      document.head.appendChild(style);
    }, 500)
  );
})();
